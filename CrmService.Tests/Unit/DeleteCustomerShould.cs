﻿using System;
using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class DeleteCustomerShould {
        private CustomerRepository customerRepository;
        private DeleteCustomer deleteCustomer;
        private CustomerQuery customerQuery;

        [SetUp]
        public void SetUp() {
            customerRepository = Substitute.For<CustomerRepository>();
            customerQuery = Substitute.For<CustomerQuery>();
            deleteCustomer = new DeleteCustomer(customerRepository, customerQuery);
        }

        [Test]
        public async Task delete_customer_from_repository() {
            var aCustomerId = "anId";
            var aCustomer = new Customer(aCustomerId, "aName", "aSurname", "anImagePath", "lastModifiedBy", "createdBy");
            customerQuery.Get(aCustomerId).Returns(aCustomer);

            await deleteCustomer.Execute(aCustomerId);

            await customerRepository.Received(1).Delete(aCustomerId);
        }

        [Test]
        public void throw_exception_if_customer_does_not_exist() {
            var aCustomerId = "anId";
            customerQuery.Get(aCustomerId).Returns(new NoCustomer());

            Func<Task> deleteCustomerAction = async () => await deleteCustomer.Execute(aCustomerId);

            deleteCustomerAction.Should().Throw<CustomerNotFoundException>().Which.Message
                .Should().Be($"customer:{aCustomerId} not found");
        }
    }
}
