﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CrmService.Business.Model;

namespace CrmService.Business.Persistance {
    public interface UserQuery {
        Task<ICollection<User>> GetAll();
        Task<User> Get(string userId);
        Task<User> Get(string userName, string password);
        Task<User> GetUser(string userName);
    }
}
