﻿namespace CrmService.Business.Model {
    public class NoToken: Token {
        public NoToken() : base(null) {
        }
    }
}
