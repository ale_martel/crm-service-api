﻿using System.Threading.Tasks;
using CrmService.Business.Model;

namespace CrmService.Business.Persistance {
    public interface CustomerRepository {
        Task<Customer> Save(Customer customer);
        Task Delete(string customerId);
    }
}
