﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class GetCustomers {
        private CustomerQuery customerQuery;

        public GetCustomers(CustomerQuery customerQuery) {
            this.customerQuery = customerQuery;
        }

        public async Task<ICollection<Customer>> Execute() {
            return await customerQuery.GetAll();
        }
    }
}
