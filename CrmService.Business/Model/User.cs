﻿using System;
using CrmService.Business.Dtos;

namespace CrmService.Business.Model {
    public class User {
        public string Id { get; }
        public string UserName { get; }
        public string Password { get; }
        public bool IsAdmin { get; private set; }

        public User(string id, string userName, string password, bool isAdmin) {
            Id = id;
            UserName = userName;
            Password = password;
            IsAdmin = isAdmin;
        }
        public static User Create(string userRequestUserName, string userRequestPassword, bool userRequestIsAdmin) {
            return new User(Guid.NewGuid().ToString(), userRequestUserName, userRequestPassword, userRequestIsAdmin);
        }

        public void SetIsAdmin(bool isAdmin) {
            IsAdmin = isAdmin;
        }

        public UserDto ToDto() {
            return new UserDto() {
                Id = Id,
                UserName = UserName,
                Password = Password,
                IsAdmin = IsAdmin
            };
        }

        public override bool Equals(object obj) {
            var user = obj as User;
            return user != null &&
                   Id == user.Id &&
                   UserName == user.UserName &&
                   Password == user.Password &&
                   IsAdmin == user.IsAdmin;
        }

        
    }
}
