﻿namespace CrmService.Business.Dtos {
    public class CustomerDto {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string ImagePath { get; set; }
        public string LastModifiedBy { get; set; }
        public string CreatedBy { get; set; }
    }
}