﻿using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Persistance;
using FluentAssertions;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using NUnit.Framework;

namespace CrmService.Tests.Integration {
    public class TableStorageTokenQueryShould {
        private const string AUserId = "anId";
        private CloudStorageAccount storageAccount;
        private TableStorageTokenQuery tableStorageTokenQuery;
        private CloudTable tokensTable;
        private readonly string tokensTableName = "tokens";

        [SetUp]
        public void SetUp() {
            storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            tableStorageTokenQuery = new TableStorageTokenQuery(storageAccount);
            var tableClient = storageAccount.CreateCloudTableClient();
            tokensTable = tableClient.GetTableReference(tokensTableName);
        }

        [TearDown]
        public async Task Teardown() {
            await tokensTable.DeleteIfExistsAsync();
        }

        [Test]
        public async Task get_token_by_user_id() {
            await tokensTable.CreateIfNotExistsAsync();
            var token = Token.Create(AUserId);
            TokenEntity.CreateTokenEntity(token);
            var aTokenEntity = TokenEntity.CreateTokenEntity(token);
            await Insert(tokensTable, aTokenEntity);

            var result = await tableStorageTokenQuery.Get(AUserId);
            
            result.JwtSecurityToken.Claims.Should().BeEquivalentTo(token.JwtSecurityToken.Claims);
        }

        [Test]
        public async Task no_token_if_token_for_user_does_not_exist() {
            await tokensTable.CreateIfNotExistsAsync();

            var token = await tableStorageTokenQuery.Get(AUserId);

            token.Should().BeOfType<NoToken>();
        }

        private static async Task Insert(CloudTable cloudTable, params ITableEntity[] entities) {
            var insertQuery = new TableBatchOperation();
            foreach (var entity in entities) {
                insertQuery.Insert(entity);
            }
            await cloudTable.ExecuteBatchAsync(insertQuery);
        }
    }
}
