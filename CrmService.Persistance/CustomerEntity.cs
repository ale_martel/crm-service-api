﻿using CrmService.Business.Model;
using Microsoft.WindowsAzure.Storage.Table;

namespace CrmService.Persistance {
    public class CustomerEntity: TableEntity {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string ImagePath { get; set; }
        public string LastModifiedBy { get; set; }
        public string CreatedBy { get; set; }

        public CustomerEntity() { }

        public CustomerEntity(string id, string name, string surname, string imagePath, string lastModifiedBy, string createdBy) {
            this.PartitionKey = "crm";
            this.RowKey = id;
            Name = name;
            Surname = surname;
            ImagePath = imagePath;
            LastModifiedBy = lastModifiedBy;
            CreatedBy = createdBy;
        }

        public Customer CreateCustomer() {
            return new Customer(RowKey, Name, Surname, ImagePath, LastModifiedBy, CreatedBy);
        }

        public static CustomerEntity CreateCustomerEntity(Customer customer) {
            return new CustomerEntity(customer.Id, customer.Name, customer.Surname, customer.ImagePath, customer.LastModifiedBy, customer.CreatedBy);
        }
    }
}
