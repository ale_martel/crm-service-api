﻿using System;

namespace CrmService.Business.Exceptions {
    public class UserAlreadyExistException : Exception {
        public UserAlreadyExistException(string message) : base(message) {
        }
    }
}
