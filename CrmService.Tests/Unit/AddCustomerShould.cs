﻿using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class AddCustomerShould {
        private CustomerRepository customerRepository;
        private AddCustomer addCustomer;

        [SetUp]
        public void SetUp() {
            customerRepository = Substitute.For<CustomerRepository>();
            addCustomer = new AddCustomer(customerRepository);
        }

        [Test]
        public async Task create_customer_in_repository() {
            var aCustomer = new Customer("anId", "aName", "aSurname", "anImagePath", "lastModifiedBy", "createdBy");

            await addCustomer.Execute(aCustomer);

            await customerRepository.Received(1).Save(aCustomer);
        }
    }
}
