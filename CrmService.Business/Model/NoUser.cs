﻿namespace CrmService.Business.Model {
    public class NoUser: User {
        public NoUser() : base(null, null, null, false) {
        }
    }
}
