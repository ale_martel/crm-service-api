﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Persistance;
using FluentAssertions;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using NUnit.Framework;

namespace CrmService.Tests.Integration {
    public class TableStorageCustomerRepositoryShould {
        private const string ACustomerId = "anId";
        private const string Name = "aName";
        private const string Surname = "aSurname";
        private const string ImagePath = "anImagePath";
        private const string LastModifiedBy = "userId";
        private const string CreatedBy = "userId";
        private CloudStorageAccount storageAccount;
        private TableStorageCustomerRepository tableStorageCustomerRepository;
        private CloudTable customersTable;
        private readonly string customersTableName = "customers";

        [SetUp]
        public void SetUp() {
            storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            tableStorageCustomerRepository = new TableStorageCustomerRepository(storageAccount);
            var tableClient = storageAccount.CreateCloudTableClient();
            customersTable = tableClient.GetTableReference(customersTableName);
        }

        [TearDown]
        public async Task Teardown() {
            await customersTable.DeleteIfExistsAsync();
        }

        [Test]
        public async Task insert_customer() {
            var aCustomer = new Customer(ACustomerId, Name, Surname, ImagePath, LastModifiedBy, CreatedBy);
            var expectedCustomer = new Customer(ACustomerId, Name, Surname, ImagePath, LastModifiedBy, CreatedBy);

            var customer = await tableStorageCustomerRepository.Save(aCustomer);

            customer.Should().BeEquivalentTo(expectedCustomer);
        }

        [Test]
        public async Task update_customer() {
            await customersTable.CreateIfNotExistsAsync();
            var anotherImage = "anotherImagePath";
            var aUserId = "aUserId";
            var aCustomer = new CustomerEntity(ACustomerId, Name, Surname, ImagePath, LastModifiedBy, CreatedBy);
            var expectedCustomer = new Customer(ACustomerId, Name, Surname, anotherImage, aUserId, CreatedBy);
            await Insert(customersTable, aCustomer);
            var customer = aCustomer.CreateCustomer();
            customer.SetCustomerImage(anotherImage, aUserId);

            await tableStorageCustomerRepository.Save(customer);

            var customers = await GetCustomers();
            customers.Count.Should().Be(1);
            customers.First().Should().BeEquivalentTo(expectedCustomer);
        }
        
        [Test]
        public async Task delete_customer() {
            await customersTable.CreateIfNotExistsAsync();
            var aCustomer = new CustomerEntity(ACustomerId, Name, Surname, ImagePath, LastModifiedBy, CreatedBy);
            await Insert(customersTable, aCustomer);

            await tableStorageCustomerRepository.Delete(ACustomerId);

            var customers = await GetCustomers();
            customers.Should().BeEmpty();
        }

        private async Task<ICollection<Customer>> GetCustomers() {
            var query = new TableQuery<CustomerEntity>();
            var customersQueryResults = await customersTable.ExecuteQuerySegmentedAsync(query, null);
            var customers = customersQueryResults.Select(entity => new Customer(entity.RowKey, entity.Name, entity.Surname, entity.ImagePath, entity.LastModifiedBy, entity.CreatedBy));
            return customers.ToList();
        }

        private static async Task Insert(CloudTable cloudTable, params ITableEntity[] entities) {
            var insertQuery = new TableBatchOperation();
            foreach (var entity in entities) {
                insertQuery.Insert(entity);
            }
            await cloudTable.ExecuteBatchAsync(insertQuery);
        }
    }
}
