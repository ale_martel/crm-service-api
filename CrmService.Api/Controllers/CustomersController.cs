﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmService.Api.Requests;
using CrmService.Business.Dtos;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Mvc.Versioning.Conventions;

namespace CrmService.Api.Controllers {
    [Route("api/v{version:apiVersion}/[controller]")]
    [UserAuthFilter]
    public class CustomersController : Controller {
        public static void Convention(ApiVersioningOptions options) {
            options.Conventions.Controller<CustomersController>().HasApiVersion(1, 0);
        }

        [HttpGet]
        public async Task<ICollection<CustomerDto>> GetCustomers() {
            var getCustomersAction = ActionFactory.CreateGetCustomers();
            return (await getCustomersAction.Execute()).Select(customer => customer.ToDto()).ToList();
        }

        [HttpGet("{customerId}")]
        public async Task<ActionResult<CustomerDto>> GetCustomer(string customerId) {
            try {
                var getCustomerAction = ActionFactory.CreateGetCustomer();
                return (await getCustomerAction.Execute(customerId)).ToDto();
            }
            catch (CustomerNotFoundException e) {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<CustomerDto> CreateCustomer([FromBody] CreateCustomerRequest customerRequest) {
            var addCustomerAction = ActionFactory.CreateAddCustomer();
            var userClaims = HttpContext.User;
            return (await addCustomerAction.Execute(Customer.Create(customerRequest.Name, customerRequest.Surname,
                customerRequest.ImagePath, userClaims.FindFirst("userId").Value, userClaims.FindFirst("userId").Value))).ToDto();
        }

        [HttpPut]
        public async Task<ActionResult<CustomerDto>> UpdateCustomer([FromBody] UpdateCustomerRequest customerDto) {
            try {
                var updateCustomerAction = ActionFactory.CreateUpdateCustomer();
                var userClaims = HttpContext.User;
                return (await updateCustomerAction.Execute(new Customer(customerDto.Id, customerDto.Name, customerDto.Surname,
                    customerDto.ImagePath, userClaims.FindFirst("userId").Value, customerDto.CreatedBy))).ToDto();
            }
            catch (CustomerNotFoundException e) {
                return NotFound();
            }
        }

        [HttpDelete("{customerId}")]
        public async Task<ActionResult> DeleteCustomer(string customerId) {
            try {
                var deleteCustomerAction = ActionFactory.CreateDeleteCustomer();
                await deleteCustomerAction.Execute(customerId);
            }
            catch (CustomerNotFoundException e) {
                return NotFound();
            }
            return NoContent();
        }
    }
}
