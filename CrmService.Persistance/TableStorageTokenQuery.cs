﻿using System.Linq;
using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace CrmService.Persistance {
    public class TableStorageTokenQuery : TokenQuery{
        private readonly CloudTable tokensTable;
        private readonly string tokensTableName = "tokens";

        public TableStorageTokenQuery(CloudStorageAccount storageAccount) {
            tokensTable = storageAccount.CreateCloudTableClient().GetTableReference(tokensTableName);
        }
        
        public virtual async Task<Token> Get(string userId) {
            await tokensTable.CreateIfNotExistsAsync();
            var query = new TableQuery<TokenEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "crm"))
                .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, userId));
            var tokensQueryResults = await tokensTable.ExecuteQuerySegmentedAsync(query, null);
            return tokensQueryResults.Select(c => c.CreateToken()).DefaultIfEmpty(new NoToken())
                .FirstOrDefault();
        }
    } 
}
