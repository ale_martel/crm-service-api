﻿using System.Threading.Tasks;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class DeleteCustomer {
        private readonly CustomerRepository customerRepository;
        private readonly CustomerQuery customerQuery;

        public DeleteCustomer(CustomerRepository customerRepository, CustomerQuery customerQuery) {
            this.customerRepository = customerRepository;
            this.customerQuery = customerQuery;
        }

        public async Task Execute(string customerId) {
            var deletingCustomer = await customerQuery.Get(customerId);
            if (deletingCustomer.GetType() == typeof(NoCustomer)) throw new CustomerNotFoundException($"customer:{customerId} not found");
            await customerRepository.Delete(customerId);
        }
    }
}
