﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace CrmService.Persistance {
    public class TableStorageCustomerQuery : CustomerQuery{
        private readonly CloudTable customersTable;
        private readonly string customersTableName = "customers";

        public TableStorageCustomerQuery(CloudStorageAccount storageAccount) {
            customersTable = storageAccount.CreateCloudTableClient().GetTableReference(customersTableName);
        }

        public async Task<ICollection<Customer>> GetAll() {
            await customersTable.CreateIfNotExistsAsync();
            var query = new TableQuery<CustomerEntity>();
            var customersQueryResults = await customersTable.ExecuteQuerySegmentedAsync(query, null);
            var customers = customersQueryResults.Select(entity => new Customer(entity.RowKey, entity.Name, entity.Surname, entity.ImagePath, entity.LastModifiedBy, entity.CreatedBy));
            return customers.ToList();
        }

        public virtual async Task<Customer> Get(string customerId) {
            await customersTable.CreateIfNotExistsAsync();
            var query = new TableQuery<CustomerEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "crm"))
                                                        .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, customerId));
            var customersQueryResults = await customersTable.ExecuteQuerySegmentedAsync(query, null);
            return customersQueryResults.Select(c => c.CreateCustomer()).DefaultIfEmpty(new NoCustomer())
                .FirstOrDefault();
        }
    }
}
