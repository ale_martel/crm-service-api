﻿using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Persistance;
using FluentAssertions;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using NUnit.Framework;

namespace CrmService.Tests.Integration {
    public class TableStorageCustomerQueryShould {
        private const string ACustomerId = "anId";
        private const string AnotherCustomerId = "anotherId";
        private const string Name = "aName";
        private const string Surname = "aSurname";
        private const string ImagePath = "anImagePath";
        private const string LastModifiedBy = "userId";
        private const string CreatedBy = "userId";
        private CloudStorageAccount storageAccount;
        private TableStorageCustomerQuery tableStorageCustomerQuery;
        private CloudTable customersTable;
        private readonly string customersTableName = "customers";

        [SetUp]
        public void SetUp() {
            storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            tableStorageCustomerQuery = new TableStorageCustomerQuery(storageAccount);
            var tableClient = storageAccount.CreateCloudTableClient();
            customersTable = tableClient.GetTableReference(customersTableName);
        }

        [TearDown]
        public async Task Teardown() {
            await customersTable.DeleteIfExistsAsync();
        }

        [Test]
        public async Task get_customer_by_id() {
            await customersTable.CreateIfNotExistsAsync();
            var aCustomerEntity = new CustomerEntity(ACustomerId, Name, Surname, ImagePath, LastModifiedBy, CreatedBy);
            await Insert(customersTable, aCustomerEntity);

            var customer = await tableStorageCustomerQuery.Get(ACustomerId);

            var expectedCustomer = aCustomerEntity.CreateCustomer();
            customer.Should().BeEquivalentTo(expectedCustomer);
        }

        [Test]
        public async Task no_customer_if_customer_does_not_exist() {
            await customersTable.CreateIfNotExistsAsync();

            var customer = await tableStorageCustomerQuery.Get(ACustomerId);
            
            customer.Should().BeOfType<NoCustomer>();
        }

        [Test]
        public async Task get_all_customers() {
            await customersTable.CreateIfNotExistsAsync();
            var aCustomerEntity = new CustomerEntity(ACustomerId, Name, Surname, ImagePath, LastModifiedBy, CreatedBy);
            var anotherCustomerEntity = new CustomerEntity(AnotherCustomerId, Name, Surname, ImagePath, LastModifiedBy, CreatedBy);
            await Insert(customersTable, aCustomerEntity, anotherCustomerEntity);

            var customers = await tableStorageCustomerQuery.GetAll();

            var expectedCustomer = aCustomerEntity.CreateCustomer();
            var anotherExpectedCustomer = anotherCustomerEntity.CreateCustomer();
            customers.Should().Contain(expectedCustomer);
            customers.Should().Contain(anotherExpectedCustomer);
        }

        private static async Task Insert(CloudTable cloudTable, params ITableEntity[] entities) {
            var insertQuery = new TableBatchOperation();
            foreach (var entity in entities) {
                insertQuery.Insert(entity);
            }
            await cloudTable.ExecuteBatchAsync(insertQuery);
        }
    }
}
