﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace CrmService.Business.Model {
    public class Token {
        public JwtSecurityToken JwtSecurityToken { get; }

        public Token(JwtSecurityToken token) {
            JwtSecurityToken = token;
        }
        public static Token Create(string userId) {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("CrmServiceSecretKey"));
            var signInCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: "CrmService", 
                audience: "CrmService", 
                claims: new List<Claim>(){new Claim("userId", userId)}, 
                expires: DateTime.UtcNow.AddMinutes(5), 
                signingCredentials: signInCredentials);
            return new Token(token);
        }

        public override bool Equals(object obj) {
            var token = obj as Token;
            return token != null &&
                   EqualityComparer<JwtSecurityToken>.Default.Equals(JwtSecurityToken, token.JwtSecurityToken);
        }
    }
}
