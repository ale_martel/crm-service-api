﻿using System;
using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class GetUserShould {
        private UserQuery userQuery;
        private GetUser getUser;

        [SetUp]
        public void SetUp() {
            userQuery = Substitute.For<UserQuery>();
            getUser = new GetUser(userQuery);
        }

        [Test]
        public async Task get_user_by_id() {
            var aUserId = "anId";

            await getUser.Execute(aUserId);

            await userQuery.Received(1).Get(aUserId);
        }

        [Test]
        public async Task get_user_by_username_and_password() {
            var userName = "aUserName";
            var password = "aPassword";

            await getUser.Execute(userName, password);

            await userQuery.Received(1).Get(userName, password);
        }

        [Test]
        public void throw_exception_if_user_does_not_exist() {
            var aUserId = "anId";
            userQuery.Get(aUserId).Returns(new NoUser());

            Func<Task> getUserAction = async () => await getUser.Execute(aUserId);

            getUserAction.Should().Throw<UserNotFoundException>().Which.Message
                .Should().Be($"user:{aUserId} not found");
        }
    }
}
