﻿using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Persistance;
using FluentAssertions;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using NUnit.Framework;

namespace CrmService.Tests.Integration {
    public class TableStorageUserQueryShould {
        private const string AUserId = "anId";
        private const string AnotherUserId = "anotherId";
        private const string UserName = "aUserName";
        private const string Password = "aPassword";
        private const bool IsAdmin = true;
        private CloudStorageAccount storageAccount;
        private TableStorageUserQuery tableStorageUserQuery;
        private CloudTable usersTable;
        private readonly string usersTableName = "users";

        [SetUp]
        public async Task SetUp() {
            storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            tableStorageUserQuery = new TableStorageUserQuery(storageAccount);
            var tableClient = storageAccount.CreateCloudTableClient();
            usersTable = tableClient.GetTableReference(usersTableName);
        }

        [TearDown]
        public async Task Teardown() {
            await usersTable.DeleteIfExistsAsync();
        }

        [Test]
        public async Task get_all_users() {
            await usersTable.CreateIfNotExistsAsync();
            var aUserEntity = new UserEntity(AUserId, UserName, Password, IsAdmin);
            var anotherUserEntity = new UserEntity(AnotherUserId, UserName, Password, IsAdmin);
            await Insert(usersTable, aUserEntity, anotherUserEntity);

            var customers = await tableStorageUserQuery.GetAll();

            var expectedUser = aUserEntity.CreateUser();
            var anotherExpectedUser = anotherUserEntity.CreateUser();
            customers.Should().Contain(expectedUser);
            customers.Should().Contain(anotherExpectedUser);
        }

        [Test]
        public async Task get_user() {
            await usersTable.CreateIfNotExistsAsync();
            var aUserEntity = new UserEntity(AUserId, UserName, Password, isAdmin: false);
            await Insert(usersTable, aUserEntity);

            var customer = await tableStorageUserQuery.Get(AUserId);

            var expectedUser = aUserEntity.CreateUser();
            customer.Should().BeEquivalentTo(expectedUser);
        }

        [Test]
        public async Task get_user_by_username_and_password() {
            await usersTable.CreateIfNotExistsAsync();
            var aUserEntity = new UserEntity(AUserId, UserName, Password, isAdmin: false);
            await Insert(usersTable, aUserEntity);

            var customer = await tableStorageUserQuery.Get(UserName, Password);

            var expectedUser = aUserEntity.CreateUser();
            customer.Should().BeEquivalentTo(expectedUser);
        }

        [Test]
        public async Task get_user_by_username() {
            await usersTable.CreateIfNotExistsAsync();
            var aUserEntity = new UserEntity(AUserId, UserName, Password, isAdmin: false);
            await Insert(usersTable, aUserEntity);

            var customer = await tableStorageUserQuery.GetUser(UserName);

            var expectedUser = aUserEntity.CreateUser();
            customer.Should().BeEquivalentTo(expectedUser);
        }

        [Test]
        public async Task no_user_if_user_does_not_exist() {
            await usersTable.CreateIfNotExistsAsync();

            var user = await tableStorageUserQuery.Get(AUserId);

            user.Should().BeOfType<NoUser>();
        }

        private static async Task Insert(CloudTable cloudTable, params ITableEntity[] entities) {
            var insertQuery = new TableBatchOperation();
            foreach (var entity in entities) {
                insertQuery.Insert(entity);
            }
            await cloudTable.ExecuteBatchAsync(insertQuery);
        }
    }
}
