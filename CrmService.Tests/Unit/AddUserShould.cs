﻿using System;
using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class AddUserShould {
        private UserRepository userRepository;
        private UserQuery userQuery;
        private AddUser addUser;

        [SetUp]
        public void SetUp() {
            userRepository = Substitute.For<UserRepository>();
            userQuery = Substitute.For<UserQuery>();
            addUser = new AddUser(userRepository, userQuery);
        }

        [Test]
        public async Task create_user_in_repository() {
            var aUser = new User("anId", "aUserName", "aPassword", isAdmin:false);

            await addUser.Execute(aUser);

            await userRepository.Received(1).Save(aUser);
        }

        [Test]
        public void throw_exception_if_user_already_exist() {
            var userName = "aUserName";
            var aUser = new User("anId", userName, "aPassword", isAdmin: false);
            userQuery.GetUser(userName).Returns(aUser);

            Func<Task> addUserAction = async () => await addUser.Execute(aUser);

            addUserAction.Should().Throw<UserAlreadyExistException>().Which.Message
                .Should().Be($"user:{userName} already exist");
        }
    }
}
