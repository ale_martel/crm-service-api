﻿namespace CrmService.Business.Dtos {
    public class UserDto {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }
}
