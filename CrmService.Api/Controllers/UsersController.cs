﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CrmService.Api.Requests;
using CrmService.Business.Dtos;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Mvc.Versioning.Conventions;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;

namespace CrmService.Api.Controllers {
    [Route("api/v{version:apiVersion}/[controller]")]
    [UserAuthFilter]
    public class UsersController : Controller {
        public static void Convention(ApiVersioningOptions options) {
            options.Conventions.Controller<CustomersController>().HasApiVersion(1, 0);
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<UserDto>>> GetUsers() {
            var userClaims = HttpContext.User;
            if (!userClaims.Claims.Any() || userClaims.FindFirst("isAdmin").Value != "True") return Unauthorized();
            var getUsersAction = ActionFactory.CreateGetUsers();
            return (await getUsersAction.Execute()).Select(user => user.ToDto()).ToList();
        }
        
        [HttpPost]
        public async Task<ActionResult<UserDto>> CreateUser([FromBody] CreateUserRequest userRequest) {
            if (!IsUserCreationOpen()) {
                var userClaims = HttpContext.User;
                if (!userClaims.Claims.Any() || userClaims.FindFirst("isAdmin").Value != "True") return Unauthorized();
            }
            try {
                var addUserAction = ActionFactory.CreateAddUser();
                return (await addUserAction.Execute(Business.Model.User.Create(userRequest.UserName,
                    userRequest.Password, userRequest.IsAdmin))).ToDto();
            }
            catch (UserAlreadyExistException e) {
                return BadRequest();
            }
        }

        [HttpPut]
        public async Task<ActionResult<UserDto>> UpdateUser([FromBody] UserDto userDto) {
            var userClaims = HttpContext.User;
            if (!userClaims.Claims.Any() || userClaims.FindFirst("isAdmin").Value != "True") return Unauthorized();
            try {
                var updateUserAction = ActionFactory.CreateUpdateUser();
                return (await updateUserAction.Execute(new User(userDto.Id, userDto.UserName, userDto.Password, userDto.IsAdmin))).ToDto();
            }
            catch (UserNotFoundException e) {
                return NotFound();
            }
        }

        [HttpPatch("{userId}")]
        public async Task<ActionResult<UserDto>> UpdateAdminStatus([FromBody] UpdateAdminStatusRequest adminStatusRequest, string userId) {
            var userClaims = HttpContext.User;
            if (!userClaims.Claims.Any() || userClaims.FindFirst("isAdmin").Value != "True") return Unauthorized();
            try {
                var updateAdminStatusAction = ActionFactory.CreateUpdateAdminStatus();
                return (await updateAdminStatusAction.Execute(userId, adminStatusRequest.IsAdmin)).ToDto();
            }
            catch (UserNotFoundException e) {
                return NotFound();
            }
        }

        [HttpDelete("{userId}")]
        public async Task<ActionResult> DeleteUser(string userId) {
            var userClaims = HttpContext.User;
            if (userClaims.FindFirst("isAdmin").Value != "True") return Unauthorized();
            try {
                var deleteUserAction = ActionFactory.CreateDeleteUser();
                await deleteUserAction.Execute(userId);
            }
            catch (UserNotFoundException e) {
                return NotFound();
            }

            return NoContent();
        }
        

        private static bool IsUserCreationOpen() {
            var configuration = BuildConfiguration();
            return bool.Parse(configuration["UserCreationOpen"]);
        }

        private static IConfigurationRoot BuildConfiguration() {
            return new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
        }
    }
}
