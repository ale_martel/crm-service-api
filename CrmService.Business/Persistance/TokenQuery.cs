﻿using System.Threading.Tasks;
using CrmService.Business.Model;

namespace CrmService.Business.Persistance {
    public interface TokenQuery {
        Task<Token> Get(string userId);
    }
}
