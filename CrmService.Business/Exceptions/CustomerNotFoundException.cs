﻿using System;

namespace CrmService.Business.Exceptions {
    public class CustomerNotFoundException : Exception {
        public CustomerNotFoundException(string message) : base(message) {
        }
    }
}
