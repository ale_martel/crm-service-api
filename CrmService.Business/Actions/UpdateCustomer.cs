﻿using System.Threading.Tasks;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class UpdateCustomer {
        private readonly CustomerRepository customerRepository;
        private readonly CustomerQuery customerQuery;

        public UpdateCustomer(CustomerRepository customerRepository, CustomerQuery customerQuery) {
            this.customerRepository = customerRepository;
            this.customerQuery = customerQuery;
        }

        public async Task<Customer> Execute(Customer customer) {
            var updatingCustomer = await customerQuery.Get(customer.Id);
            if (updatingCustomer.GetType() == typeof(NoCustomer)) throw new CustomerNotFoundException($"customer:{customer.Id} not found");
            return await customerRepository.Save(customer);
        }
    }
}
