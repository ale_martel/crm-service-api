﻿namespace CrmService.Api.Requests {
    public class CreateUserRequest {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }
}
