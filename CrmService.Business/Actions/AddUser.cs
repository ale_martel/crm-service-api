﻿using System.Threading.Tasks;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class AddUser {
        private readonly UserRepository userRepository;
        private readonly UserQuery userQuery;

        public AddUser(UserRepository userRepository, UserQuery userQuery) {
            this.userRepository = userRepository;
            this.userQuery = userQuery;
        }
        public async Task<User> Execute(User user) {
            if ((await userQuery.GetUser(user.UserName)).GetType() != typeof(NoUser)) throw new UserAlreadyExistException($"user:{user.UserName} already exist");
            return await userRepository.Save(user);
        }
    }
}
