﻿using System;

namespace CrmService.Business.Exceptions {
    public class UserNotFoundException : Exception {
        public UserNotFoundException(string message) : base(message) {
        }
    }
}
