crm-service-api  
  
The API is developed in ASP.NET.Core 2.1  
  
Design patterns:  
- Domain Driven Design  
- Comamand Query Responsability Segregation (regarding the code design not the infrastructure)  
- Hexagonal Architecture  
  
Development metodologies:  
- SOLID Principles  
- TDD  
  
Deployed using AzureDevops in Azure Cloud -> https://crm-service-api.azurewebsites.net/api/v1  
  
Persistance of the data in Azure Table Storage  
For development:  
Azure Storage Emulator -> https://go.microsoft.com/fwlink/?LinkId=717179&clcid=0x40a  
Azure Storage Explorer -> https://azure.microsoft.com/es-es/features/storage-explorer/  
  
OAuth:  
The api has a an endpoint for generating JWT tokens, this token is the identification of the user and is needed to be  
sent as a oAuth2 access token to the rest of endpoints, it has 5 minutes of expiration time.  
  
Settings:  
StorageConnectionString: this is the connection string to the azure storage account where you want to save the data  
UserCreationOpen: this is a flag, if it's set to false it removes the authorization check for the endpoint to  
create users (for creating the first admin it should be turned false)
  
  
API: In the root of the repo there is a postman collection with all the endpoints    
![alt text][api]
[api]: ./api.png "API"

