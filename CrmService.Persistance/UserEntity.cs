﻿using CrmService.Business.Model;
using Microsoft.WindowsAzure.Storage.Table;

namespace CrmService.Persistance {
    public class UserEntity: TableEntity {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }

        public UserEntity() { }

        public UserEntity(string id, string userName, string password, bool isAdmin) {
            this.PartitionKey = "crm";
            this.RowKey = id;
            UserName = userName;
            Password = password;
            IsAdmin = isAdmin;
        }

        public User CreateUser() {
            return new User(RowKey, UserName, Password, IsAdmin);
        }

        public static UserEntity CreateUserEntity(User customer) {
            return new UserEntity(customer.Id, customer.UserName, customer.Password, customer.IsAdmin);
        }
    }
}
