﻿using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Persistance;
using FluentAssertions;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using NUnit.Framework;

namespace CrmService.Tests.Integration {
    public class TableStorageTokenRepositoryShould {
        private const string AUserId = "anId";
        private const string ATokenValue = "aTokenValue";
        private CloudStorageAccount storageAccount;
        private TableStorageTokenRepository tableStorageTokenRepository;
        private CloudTable tokensTable;
        private const string TokensTableName = "tokens";

        [SetUp]
        public void SetUp() {
            storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            tableStorageTokenRepository = new TableStorageTokenRepository(storageAccount);
            var tableClient = storageAccount.CreateCloudTableClient();
            tokensTable = tableClient.GetTableReference(TokensTableName);
        }

        [TearDown]
        public async Task Teardown() {
            await tokensTable.DeleteIfExistsAsync();
        }

        [Test]
        public async Task insert_token() {
            var aToken = Token.Create(AUserId);

            await tableStorageTokenRepository.Save(aToken);

            var tokens = await GetTokens();
            tokens.Count().Should().Be(1);
            tokens.First().JwtSecurityToken.Claims.Should().BeEquivalentTo(aToken.JwtSecurityToken.Claims);
        }

        [Test]
        public async Task update_token_if_already_exist_a_token_for_that_user() {
            await tokensTable.CreateIfNotExistsAsync();
            var aTokenEntity = new TokenEntity(AUserId, ATokenValue);
            await Insert(tokensTable, aTokenEntity);
            var anotherToken = Token.Create(AUserId);

            await tableStorageTokenRepository.Save(anotherToken);

            var tokens = await GetTokens();
            tokens.Count.Should().Be(1);
            tokens.First().JwtSecurityToken.Claims.Should().BeEquivalentTo(anotherToken.JwtSecurityToken.Claims);
        }
        
        private async Task<ICollection<Token>> GetTokens() {
            var query = new TableQuery<TokenEntity>();
            var tokensQueryResults = await tokensTable.ExecuteQuerySegmentedAsync(query, null);
            var tokens = tokensQueryResults.Select(entity => new Token(new JwtSecurityToken(entity.Value)));
            return tokens.ToList();
        }

        private static async Task Insert(CloudTable cloudTable, params ITableEntity[] entities) {
            var insertQuery = new TableBatchOperation();
            foreach (var entity in entities) {
                insertQuery.Insert(entity);
            }
            await cloudTable.ExecuteBatchAsync(insertQuery);
        }
    }
}
