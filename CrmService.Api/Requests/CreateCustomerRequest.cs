﻿namespace CrmService.Api.Requests {
    public class CreateCustomerRequest {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string ImagePath { get; set; }
    }
}