﻿using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Persistance;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class GetTokenShould {
        private TokenQuery tokenQuery;
        private GetToken getToken;

        [SetUp]
        public void SetUp() {
            tokenQuery = Substitute.For<TokenQuery>();
            getToken = new GetToken(tokenQuery);
        }

        [Test]
        public async Task get_token_by_user_id() {
            var aUserId = "anId";

            await getToken.Execute(aUserId);

            await tokenQuery.Received(1).Get(aUserId);
        }
    }
}
