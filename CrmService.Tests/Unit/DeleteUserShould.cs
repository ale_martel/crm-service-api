﻿using System;
using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class DeleteUserShould {
        private UserRepository userRepository;
        private DeleteUser deleteUser;
        private UserQuery userQuery;

        [SetUp]
        public void SetUp() {
            userRepository = Substitute.For<UserRepository>();
            userQuery = Substitute.For<UserQuery>();
            deleteUser = new DeleteUser(userRepository, userQuery);
        }

        [Test]
        public async Task delete_user_from_repository() {
            var aUserId = "anId";
            var aUser = new User(aUserId, "aName", "aPassword", isAdmin: false);
            userQuery.Get(aUserId).Returns(aUser);

            await deleteUser.Execute(aUserId);

            await userRepository.Received(1).Delete(aUserId);
        }

        [Test]
        public void throw_exception_if_user_does_not_exist() {
            var aUserId = "anId";
            userQuery.Get(aUserId).Returns(new NoUser());

            Func<Task> updateUserAction = async () => await deleteUser.Execute(aUserId);

            updateUserAction.Should().Throw<UserNotFoundException>().Which.Message
                .Should().Be($"user:{aUserId} not found");
        }
    }
}
