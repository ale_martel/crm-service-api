﻿using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Persistance;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class GetCustomerShould {
        private CustomerQuery customerQuery;
        private GetCustomer getCustomer;

        [SetUp]
        public void SetUp() {
            customerQuery = Substitute.For<CustomerQuery>();
            getCustomer = new GetCustomer(customerQuery);
        }

        [Test]
        public async Task get_customer_by_id() {
            var aCustomer = "anId";

            await getCustomer.Execute(aCustomer);

            await customerQuery.Received(1).Get(aCustomer);
        }
    }
}
