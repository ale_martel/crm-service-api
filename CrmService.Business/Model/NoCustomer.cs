﻿namespace CrmService.Business.Model {
    public class NoCustomer: Customer {
        public NoCustomer() : base(null, null, null, null, null, null) {
        }
    }
}
