﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class GetUsers {
        private UserQuery userQuery;

        public GetUsers(UserQuery userQuery) {
            this.userQuery = userQuery;
        }

        public async Task<ICollection<User>> Execute() {
            return await userQuery.GetAll();
        }
    }
}
