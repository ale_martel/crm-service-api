﻿using System.Threading.Tasks;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class UpdateUser {
        private UserRepository userRepository;
        private UserQuery userQuery;

        public UpdateUser(UserRepository userRepository, UserQuery userQuery) {
            this.userRepository = userRepository;
            this.userQuery = userQuery;
        }

        public async Task<User> Execute(User user) {
            var updatingUser = await userQuery.Get(user.Id);
            if (updatingUser.GetType() == typeof(NoUser)) throw new UserNotFoundException($"user:{user.Id} not found");
            return await userRepository.Save(user);
        }
    }
}
