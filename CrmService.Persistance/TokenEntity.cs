﻿using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using CrmService.Business.Model;
using Microsoft.WindowsAzure.Storage.Table;

namespace CrmService.Persistance {
    public class TokenEntity: TableEntity {
        public string Value { get; set; }

        public TokenEntity() { }

        public TokenEntity(string userId, string tokenValue) {
            this.PartitionKey = "crm";
            this.RowKey = userId;
            Value = tokenValue;
        }

        public Token CreateToken() {
            return new Token(new JwtSecurityToken(Value));
        }

        public static TokenEntity CreateTokenEntity(Token token) {
            var userIdClaim = token.JwtSecurityToken.Claims.First(claim => claim.Type == "userId");
            var handler = new JwtSecurityTokenHandler();
            var jwt = handler.WriteToken(token.JwtSecurityToken);
            return new TokenEntity(userIdClaim.Value, jwt);
        }
    }
}
