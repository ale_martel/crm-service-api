﻿using System.IO;
using CrmService.Business.Actions;
using CrmService.Persistance;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;

namespace CrmService.Api {
    public class ActionFactory {
        public static AddCustomer CreateAddCustomer() {
            return new AddCustomer(new TableStorageCustomerRepository(GetStorageAccount()));
        }

        public static DeleteCustomer CreateDeleteCustomer() {
            return new DeleteCustomer(new TableStorageCustomerRepository(GetStorageAccount()), new TableStorageCustomerQuery(GetStorageAccount()));
        }

        public static UpdateCustomer CreateUpdateCustomer() {
            return new UpdateCustomer(new TableStorageCustomerRepository(GetStorageAccount()), new TableStorageCustomerQuery(GetStorageAccount()));
        }

        public static GetCustomer CreateGetCustomer() {
            return new GetCustomer(new TableStorageCustomerQuery(GetStorageAccount()));
        }

        public static GetCustomers CreateGetCustomers() {
            return new GetCustomers(new TableStorageCustomerQuery(GetStorageAccount()));
        }

        public static AddUser CreateAddUser() {
            return new AddUser(new TableStorageUserRepository(GetStorageAccount()), new TableStorageUserQuery(GetStorageAccount()));
        }

        public static DeleteUser CreateDeleteUser() {
            return new DeleteUser(new TableStorageUserRepository(GetStorageAccount()), new TableStorageUserQuery(GetStorageAccount()));
        }

        public static UpdateUser CreateUpdateUser() {
            return new UpdateUser(new TableStorageUserRepository(GetStorageAccount()), new TableStorageUserQuery(GetStorageAccount()));
        }

        public static GetUser CreateGetUser() {
            return new GetUser(new TableStorageUserQuery(GetStorageAccount()));
        }

        public static GetUsers CreateGetUsers() {
            return new GetUsers(new TableStorageUserQuery(GetStorageAccount()));
        }

        public static UpdateAdminStatus CreateUpdateAdminStatus() {
            return new UpdateAdminStatus(new TableStorageUserRepository(GetStorageAccount()), new TableStorageUserQuery(GetStorageAccount()));
        }

        public static GetToken CreateGetToken() {
            return new GetToken(new TableStorageTokenQuery(GetStorageAccount()));
        }

        public static AddToken CreateAddToken() {
            return new AddToken(new TableStorageTokenRepository(GetStorageAccount()));
        }

        private static CloudStorageAccount GetStorageAccount() {
            var configuration = BuildConfiguration();
            var storageAccount = CloudStorageAccount.Parse(configuration["StorageConnectionString"]);
            return storageAccount;
        }

        private static IConfigurationRoot BuildConfiguration() {
            return new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
        }
    }
}
