﻿using System;

namespace CrmService.Business.Exceptions {
    public class TokenNotFoundException : Exception {
        public TokenNotFoundException(string message) : base(message) {
        }
    }
}
