﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CrmService.Business.Model;

namespace CrmService.Business.Persistance {
    public interface CustomerQuery {
        Task<ICollection<Customer>> GetAll();
        Task<Customer> Get(string customerId);
    }
}
