﻿using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace CrmService.Persistance {
    public class TableStorageTokenRepository: TokenRepository{
        private readonly CloudTable tokensTable;
        private readonly string tokensTableName = "tokens";

        public TableStorageTokenRepository(CloudStorageAccount storageAccount) {
            tokensTable = storageAccount.CreateCloudTableClient().GetTableReference(tokensTableName);
        }

        public async Task<Token> Save(Token token) {
            await tokensTable.CreateIfNotExistsAsync();
            var tokenEntity = TokenEntity.CreateTokenEntity(token);
            TableOperation insertOperation = TableOperation.InsertOrReplace(tokenEntity);
            await tokensTable.ExecuteAsync(insertOperation);
            return token;
        }
    }
}
