﻿using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Persistance;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class GetCustomersShould {
        private CustomerQuery customerQuery;
        private GetCustomers getCustomers;

        [SetUp]
        public void SetUp() {
            customerQuery = Substitute.For<CustomerQuery>();
            getCustomers = new GetCustomers(customerQuery);
        }

        [Test]
        public async Task get_all_customers() {

            await getCustomers.Execute();

            await customerQuery.Received(1).GetAll();
        }
    }
}
