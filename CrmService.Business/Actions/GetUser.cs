﻿using System.Threading.Tasks;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class GetUser {
        private UserQuery userQuery;

        public GetUser(UserQuery userQuery) {
            this.userQuery = userQuery;
        }

        public async Task<User> Execute(string userId) {
            var user = await userQuery.Get(userId);
            if (user != null && user.GetType() == typeof(NoUser)) throw new UserNotFoundException($"user:{userId} not found");
            return user;
        }

        public async Task<User> Execute(string userName, string password) {
            var user = await userQuery.Get(userName, password);
            if (user != null && user.GetType() == typeof(NoUser)) throw new UserNotFoundException($"user not found, incorrect username or password");
            return user;
        }
    }
}
