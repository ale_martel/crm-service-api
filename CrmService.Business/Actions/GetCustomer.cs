﻿using System.Threading.Tasks;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class GetCustomer {
        private CustomerQuery customerQuery;

        public GetCustomer(CustomerQuery customerQuery) {
            this.customerQuery = customerQuery;
        }

        public async Task<Customer> Execute(string customerId) {
            var customer = await customerQuery.Get(customerId);
            if (customer != null && customer.GetType() == typeof(NoCustomer)) throw new CustomerNotFoundException($"customer:{customerId} not found");
            return customer;
        }
    }
}
