﻿using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class AddToken {
        private readonly TokenRepository tokenRepository;

        public AddToken(TokenRepository tokenRepository) {
            this.tokenRepository = tokenRepository;
        }
        public async Task<Token> Execute(string userId) {
            var token = Token.Create(userId);
            return await tokenRepository.Save(token);
        }
    }
}
