﻿using System;
using CrmService.Business.Dtos;

namespace CrmService.Business.Model {
    public class Customer {
        public string Id { get; }
        public string Name { get; }
        public string Surname { get; }
        public string ImagePath { get; private set; }
        public string LastModifiedBy { get; private set; }
        public string CreatedBy{ get; private set; }

        public Customer(string id, string name, string surname, string imagePath, string lastModifiedBy, string createdBy) {
            Id = id;
            Name = name;
            Surname = surname;
            ImagePath = imagePath;
            LastModifiedBy = lastModifiedBy;
            CreatedBy = createdBy;
        }

        public static Customer Create(string customerRequestName, string customerRequestSurname, string customerRequestImagePath, string customerRequestLastModifiedBy, string customerRequestCreatedBy) {
            return new Customer(Guid.NewGuid().ToString(), customerRequestName, customerRequestSurname, customerRequestImagePath, customerRequestLastModifiedBy, customerRequestCreatedBy);
        }

        public void SetCustomerImage(string anotherImage, string lastModifiedBy){
            ImagePath = anotherImage;
            LastModifiedBy = lastModifiedBy;
        }

        public CustomerDto ToDto() {
            return new CustomerDto {
                Id = Id,
                Name = Name,
                Surname = Surname,
                ImagePath = ImagePath,
                LastModifiedBy = LastModifiedBy,
                CreatedBy = CreatedBy,
            };
        }

        public override bool Equals(object obj) {
            var customer = obj as Customer;
            return customer != null &&
                   Id == customer.Id &&
                   Name == customer.Name &&
                   Surname == customer.Surname &&
                   ImagePath == customer.ImagePath &&
                   LastModifiedBy == customer.LastModifiedBy &&
                   CreatedBy == customer.CreatedBy;
        }
    }
}
