﻿using System.Threading.Tasks;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class GetToken {
        private readonly TokenQuery tokenQuery;

        public GetToken(TokenQuery tokenQuery) {
            this.tokenQuery = tokenQuery;
        }

        public async Task<Token> Execute(string userId) {
            var token = await tokenQuery.Get(userId);
            if (token != null && token.GetType() == typeof(NoToken)) throw new TokenNotFoundException($"Token not found for user:{userId}");
            return token;
        }
    }
}
