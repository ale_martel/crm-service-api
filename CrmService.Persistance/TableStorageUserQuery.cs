﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace CrmService.Persistance {
    public class TableStorageUserQuery : UserQuery{
        private readonly CloudTable usersTable;
        private readonly string usersTableName = "users";

        public TableStorageUserQuery(CloudStorageAccount storageAccount) {
            usersTable = storageAccount.CreateCloudTableClient().GetTableReference(usersTableName);
        }

        public async Task<ICollection<User>> GetAll() {
            await usersTable.CreateIfNotExistsAsync();
            var query = new TableQuery<UserEntity>();
            var usersQueryResults = await usersTable.ExecuteQuerySegmentedAsync(query, null);
            var users = usersQueryResults.Select(entity => new User(entity.RowKey, entity.UserName, entity.Password, entity.IsAdmin));
            return users.ToList();
        }

        public virtual async Task<User> Get(string userId) {
            await usersTable.CreateIfNotExistsAsync();
            var query = new TableQuery<UserEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "crm"))
                .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, userId));
            var usersQueryResults = await usersTable.ExecuteQuerySegmentedAsync(query, null);
            return usersQueryResults.Select(c => c.CreateUser()).DefaultIfEmpty(new NoUser())
                .FirstOrDefault();
        }

        public async Task<User> Get(string userName, string password) {
            await usersTable.CreateIfNotExistsAsync();
            var query = new TableQuery<UserEntity>().Where(TableQuery.GenerateFilterCondition("UserName", QueryComparisons.Equal, userName))
                                                    .Where(TableQuery.GenerateFilterCondition("Password", QueryComparisons.Equal, password));
            var usersQueryResults = await usersTable.ExecuteQuerySegmentedAsync(query, null);
            return usersQueryResults.Select(c => c.CreateUser()).DefaultIfEmpty(new NoUser())
                .FirstOrDefault();
        }

        public async Task<User> GetUser(string userName) {
            await usersTable.CreateIfNotExistsAsync();
            var query = new TableQuery<UserEntity>().Where(TableQuery.GenerateFilterCondition("UserName", QueryComparisons.Equal, userName));
            var usersQueryResults = await usersTable.ExecuteQuerySegmentedAsync(query, null);
            return usersQueryResults.Select(c => c.CreateUser()).DefaultIfEmpty(new NoUser())
                .FirstOrDefault();
        }
    } 
}
