﻿namespace CrmService.Api.Requests {
    public class UpdateAdminStatusRequest {
        public bool IsAdmin { get; set; }
    }
}
