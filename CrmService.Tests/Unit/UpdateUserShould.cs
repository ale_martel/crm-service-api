﻿using System;
using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class UpdateUserShould {
        private UserRepository userRepository;
        private UpdateUser updateUser;
        private UserQuery userQuery;

        [SetUp]
        public async Task SetUp() {
            userRepository = Substitute.For<UserRepository>();
            userQuery = Substitute.For<UserQuery>();
            updateUser = new UpdateUser(userRepository, userQuery);
        }

        [Test]
        public async Task update_user_in_repository() {
            var aUserId = "anId";
            var aUser = new User(aUserId, "aUserName", "aPassword", isAdmin:false);
            userQuery.Get(aUserId).Returns(aUser);
            aUser.SetIsAdmin(true);

            await updateUser.Execute(aUser);

            await userRepository.Received(1).Save(aUser);
        }

        [Test]
        public void throw_exception_if_user_does_not_exist() {
            var aUserId = "anId";
            var aUser = new User(aUserId, "aUserName", "aPassword", isAdmin: false);
            userQuery.Get(aUserId).Returns(new NoUser());

            Func<Task> updateUserAction = async () => await updateUser.Execute(aUser);

            updateUserAction.Should().Throw<UserNotFoundException>().Which.Message
                .Should().Be($"user:{aUserId} not found");
        }
    }
}
