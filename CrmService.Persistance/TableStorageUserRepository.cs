﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace CrmService.Persistance {
    public class TableStorageUserRepository: UserRepository{
        private readonly CloudTable usersTable;
        private readonly string usersTableName = "users";

        public TableStorageUserRepository(CloudStorageAccount storageAccount) {
            usersTable = storageAccount.CreateCloudTableClient().GetTableReference(usersTableName);
        }

        public async Task<User> Save(User user) {
            await usersTable.CreateIfNotExistsAsync();
            var userEntity = UserEntity.CreateUserEntity(user);
            TableOperation insertOperation = TableOperation.InsertOrReplace(userEntity);
            await usersTable.ExecuteAsync(insertOperation);
            return user;
        }

        public async Task Delete(string userId) {
            var query = new TableQuery<CustomerEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "crm"))
                                                        .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, userId));
            var usersQueryResults = await usersTable.ExecuteQuerySegmentedAsync(query, null);
            var user = usersQueryResults.Single();
            TableOperation deleteOperation = TableOperation.Delete(user);
            await usersTable.ExecuteAsync(deleteOperation);
        }
    }
}
