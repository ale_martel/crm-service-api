﻿using System.Threading.Tasks;
using CrmService.Business.Model;

namespace CrmService.Business.Persistance {
    public interface TokenRepository {
        Task<Token> Save(Token token);
    }
}
