﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using CrmService.Business.Model;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CrmService.Api {
    public class UserAuthFilterAttribute: ActionFilterAttribute {

        public override void OnActionExecuting(ActionExecutingContext context) {
            var jwtToken = GetJwtTokenFrom(context);
            var validTo = jwtToken.ValidTo;
            if (validTo < DateTime.UtcNow) {
                context.HttpContext.Response.StatusCode = 401;
                return;
            }
            var claimsIdentity = new ClaimsIdentity(jwtToken.Claims);
            var user = GetUser(claimsIdentity).GetAwaiter().GetResult();
            context.HttpContext.User = GetUserClaims(claimsIdentity, user);
        }

        private static JwtSecurityToken GetJwtTokenFrom(ActionExecutingContext context) {
            string bearerToken = context.HttpContext.Request.Headers["Authorization"];
            var accessToken = bearerToken.Substring("Bearer".Length).TrimStart();
            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            return jwtSecurityTokenHandler.ReadJwtToken(accessToken);
        }

        private static async Task<User> GetUser(ClaimsIdentity claimsIdentity) {
            var getUserAction = ActionFactory.CreateGetUser();
            var userId = claimsIdentity.FindFirst("userId").Value;
            return await getUserAction.Execute(userId);
        }

        private ClaimsPrincipal GetUserClaims(ClaimsIdentity claimsIdentity, User user) {
            claimsIdentity.AddClaim(new Claim("isAdmin", user.IsAdmin.ToString()));
            return new ClaimsPrincipal(claimsIdentity);
        }

    }
}
