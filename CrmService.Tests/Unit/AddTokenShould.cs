﻿using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using FluentAssertions;
using NSubstitute;
using NSubstitute.Core.Arguments;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class AddTokenShould {
        private TokenRepository tokenRepository;
        private AddToken addToken;

        [SetUp]
        public void SetUp() {
            tokenRepository = Substitute.For<TokenRepository>();
            addToken = new AddToken(tokenRepository);
        }

        [Test]
        public async Task create_user_in_repository() {
            var aUserid = "aUserId";
            var aToken = Token.Create(aUserid);

            await addToken.Execute(aUserid);

            await tokenRepository.Received(1).Save(Arg.Any<Token>());
        }
    }
}
