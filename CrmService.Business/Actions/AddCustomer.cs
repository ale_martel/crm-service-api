﻿using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class AddCustomer {

        private readonly CustomerRepository customerRepository;

        public AddCustomer(CustomerRepository customerRepository) {
            this.customerRepository = customerRepository;
        }

        public async Task<Customer> Execute(Customer customer) {
            return await customerRepository.Save(customer);
        }
    }
}
