﻿using System;
using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class UpdateCustomerShould {
        private CustomerRepository customerRepository;
        private UpdateCustomer updateCustomer;
        private CustomerQuery customerQuery;

        [SetUp]
        public void SetUp() {
            customerRepository = Substitute.For<CustomerRepository>();
            customerQuery = Substitute.For<CustomerQuery>();
            updateCustomer = new UpdateCustomer(customerRepository, customerQuery);
        }

        [Test]
        public async Task update_customer_in_repository() {
            var aCustomerId = "anId";
            var aCustomer = new Customer(aCustomerId, "aName", "aSurname", "anImagePath", "lastModifiedBy", "createdBy");
            customerQuery.Get(aCustomerId).Returns(aCustomer);
            aCustomer.SetCustomerImage("anotherPath", "userId");

            await updateCustomer.Execute(aCustomer);

            await customerRepository.Received(1).Save(aCustomer);
        }

        [Test]
        public void throw_exception_if_customer_does_not_exist() {
            var aCustomerId = "anId";
            var aCustomer = new Customer(aCustomerId, "aName", "aSurname", "anImagePath", "lastModifiedBy", "createdBy");
            customerQuery.Get(aCustomerId).Returns(new NoCustomer());

            Func<Task> updateCustomerAction = async () => await updateCustomer.Execute(aCustomer);

            updateCustomerAction.Should().Throw<CustomerNotFoundException>().Which.Message
                .Should().Be($"customer:{aCustomerId} not found");
        }
    }
}
