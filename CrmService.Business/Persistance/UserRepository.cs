﻿using System.Threading.Tasks;
using CrmService.Business.Model;

namespace CrmService.Business.Persistance {
    public interface UserRepository {
        Task<User> Save(User user);
        Task Delete(string userId);
    }
}
