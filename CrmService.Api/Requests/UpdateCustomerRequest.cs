﻿namespace CrmService.Api.Requests {
    public class UpdateCustomerRequest {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string ImagePath { get; set; }
        public string CreatedBy { get; set; }
    }
}