﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Persistance;
using FluentAssertions;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using NUnit.Framework;

namespace CrmService.Tests.Integration {
    public class TableStorageUserRepositoryShould {
        private const string AUserId = "anId";
        private const string UserName = "aName";
        private const string Password = "aPassword";
        private const bool IsAdmin = true;
        private CloudStorageAccount storageAccount;
        private TableStorageUserRepository tableStorageUserRepository;
        private CloudTable usersTable;
        private const string UsersTableName = "users";

        [SetUp]
        public void SetUp() {
            storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
            tableStorageUserRepository = new TableStorageUserRepository(storageAccount);
            var tableClient = storageAccount.CreateCloudTableClient();
            usersTable = tableClient.GetTableReference(UsersTableName);
        }

        [TearDown]
        public async Task Teardown() {
            await usersTable.DeleteIfExistsAsync();
        }

        [Test]
        public async Task insert_user() {
            var aUser = new User(AUserId, UserName, Password, IsAdmin);
            var expectedUser = new User(AUserId, UserName, Password, IsAdmin);

            await tableStorageUserRepository.Save(aUser);

            var users = await GetUsers();
            users.Count.Should().Be(1);
            users.First().Should().BeEquivalentTo(expectedUser);
        }

        [Test]
        public async Task update_user() {
            await usersTable.CreateIfNotExistsAsync();
            var aUser = new UserEntity(AUserId, UserName, Password, IsAdmin);
            var expectedUser = new User(AUserId, UserName, Password, isAdmin: false);
            await Insert(usersTable, aUser);
            var user = aUser.CreateUser();
            user.SetIsAdmin(false);

            await tableStorageUserRepository.Save(user);

            var users = await GetUsers();
            users.Count.Should().Be(1);
            users.First().Should().BeEquivalentTo(expectedUser);
        }

        [Test]
        public async Task delete_user() {
            await usersTable.CreateIfNotExistsAsync();
            var aUser = new UserEntity(AUserId, UserName, Password, IsAdmin);
            await Insert(usersTable, aUser);

            await tableStorageUserRepository.Delete(AUserId);

            var users = await GetUsers();
            users.Should().BeEmpty();
        }

        private async Task<ICollection<User>> GetUsers() {
            var query = new TableQuery<UserEntity>();
            var usersQueryResults = await usersTable.ExecuteQuerySegmentedAsync(query, null);
            var users = usersQueryResults.Select(entity => new User(entity.RowKey, entity.UserName, entity.Password, entity.IsAdmin));
            return users.ToList();
        }

        private static async Task Insert(CloudTable cloudTable, params ITableEntity[] entities) {
            var insertQuery = new TableBatchOperation();
            foreach (var entity in entities) {
                insertQuery.Insert(entity);
            }
            await cloudTable.ExecuteBatchAsync(insertQuery);
        }
    }
}
