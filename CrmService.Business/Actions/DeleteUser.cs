﻿using System.Threading.Tasks;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class DeleteUser {
        private UserRepository userRepository;
        private UserQuery userQuery;

        public DeleteUser(UserRepository userRepository, UserQuery userQuery) {
            this.userRepository = userRepository;
            this.userQuery = userQuery;
        }

        public async Task Execute(string userId) {
            var deletingCustomer = await userQuery.Get(userId);
            if (deletingCustomer.GetType() == typeof(NoUser)) throw new UserNotFoundException($"user:{userId} not found");
            await userRepository.Delete(userId);
        }
    }
}
