﻿using System.Threading.Tasks;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;

namespace CrmService.Business.Actions {
    public class UpdateAdminStatus {
        private UserRepository userRepository;
        private UserQuery userQuery;

        public UpdateAdminStatus(UserRepository userRepository, UserQuery userQuery) {
            this.userRepository = userRepository;
            this.userQuery = userQuery;
        }

        public async Task<User> Execute(string userId, bool newAdminStatus) {
            var updatingUser = await userQuery.Get(userId);
            if (updatingUser.GetType() == typeof(NoUser)) throw new UserNotFoundException($"user:{userId} not found");
            updatingUser.SetIsAdmin(newAdminStatus);
            return await userRepository.Save(updatingUser);
        }
    }
}
