﻿using System.Linq;
using System.Threading.Tasks;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace CrmService.Persistance {
    public class TableStorageCustomerRepository: CustomerRepository{
        private readonly CloudTable customersTable;
        private readonly string customersTableName = "customers";

        public TableStorageCustomerRepository(CloudStorageAccount storageAccount) {
            customersTable = storageAccount.CreateCloudTableClient().GetTableReference(customersTableName);
        }

        public async Task<Customer> Save(Customer customer) {
            await customersTable.CreateIfNotExistsAsync();
            var customerEntity = CustomerEntity.CreateCustomerEntity(customer);
            TableOperation insertOperation = TableOperation.InsertOrReplace(customerEntity);
            await customersTable.ExecuteAsync(insertOperation);
            return customer;
        }

        public async Task Delete(string customerId) {
            var query = new TableQuery<CustomerEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "crm"))
                                                        .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, customerId));
            var customersQueryResults = await customersTable.ExecuteQuerySegmentedAsync(query, null);
            var customer = customersQueryResults.Single();
            TableOperation deleteOperation = TableOperation.Delete(customer);
            await customersTable.ExecuteAsync(deleteOperation);
        }
    }
}
