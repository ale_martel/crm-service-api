﻿using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Persistance;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class GetUsersShould {
        private UserQuery userQuery;
        private GetUsers getUsers;

        [SetUp]
        public void SetUp() {
            userQuery = Substitute.For<UserQuery>();
            getUsers = new GetUsers(userQuery);
        }

        [Test]
        public async Task get_all_users() {

            await getUsers.Execute();

            await userQuery.Received(1).GetAll();
        }
    }
}
