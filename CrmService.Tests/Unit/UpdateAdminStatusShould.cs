﻿using System;
using System.Threading.Tasks;
using CrmService.Business.Actions;
using CrmService.Business.Exceptions;
using CrmService.Business.Model;
using CrmService.Business.Persistance;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;

namespace CrmService.Tests.Unit {
    public class UpdateAdminStatusShould {
        private UserRepository userRepository;
        private UpdateAdminStatus updateAdminStatus;
        private UserQuery userQuery;

        [SetUp]
        public void SetUp() {
            userRepository = Substitute.For<UserRepository>();
            userQuery = Substitute.For<UserQuery>();
            updateAdminStatus = new UpdateAdminStatus(userRepository, userQuery);
        }

        [Test]
        public async Task update_admin_status_of_a_user() {
            var aUserId = "anId";
            var aUser = new User(aUserId, "aUserName", "aPassword", isAdmin:false);
            userQuery.Get(aUserId).Returns(aUser);

            await updateAdminStatus.Execute(aUserId, newAdminStatus: true);

            var expectedUser = new User(aUserId, "aUserName", "aPassword", isAdmin: true);
            await userRepository.Received(1).Save(aUser);
        }

        [Test]
        public void throw_exception_if_user_does_not_exist() {
            var aUserId = "anId";
            var aUser = new User(aUserId, "aUserName", "aPassword", isAdmin: false);
            userQuery.Get(aUserId).Returns(new NoUser());

            Func<Task> updateUserAction = async () => await updateAdminStatus.Execute(aUserId, newAdminStatus: true);

            updateUserAction.Should().Throw<UserNotFoundException>().Which.Message
                .Should().Be($"user:{aUserId} not found");
        }
    }
}
