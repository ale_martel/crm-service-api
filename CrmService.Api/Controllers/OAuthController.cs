﻿using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using CrmService.Api.Requests;
using CrmService.Business.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Mvc.Versioning.Conventions;

namespace CrmService.Api.Controllers {
    [Route("api/v{version:apiVersion}/[controller]")]
    public class OAuthController : Controller {
        public static void Convention(ApiVersioningOptions options) {
            options.Conventions.Controller<OAuthController>().HasApiVersion(1, 0);
        }
        
        [HttpPost("tokens")]
        public async Task<ActionResult<string>> GenerateToken([FromBody]LoginRequest loginRequest) {
            try {
                var getUserAction = ActionFactory.CreateGetUser();
                var user = await getUserAction.Execute(loginRequest.UserName, loginRequest.Password);
                var addTokenAction = ActionFactory.CreateAddToken();
                var token = await addTokenAction.Execute(user.Id);
                var handler = new JwtSecurityTokenHandler();
                var jwt = handler.WriteToken(token.JwtSecurityToken);
                return jwt;
            }
            catch (UserNotFoundException e) {
                return NotFound();
            }
        }
    }
}
